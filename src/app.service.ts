import { Injectable } from '@nestjs/common';
import { log } from 'console';
import * as jwt from 'jsonwebtoken';
import axios from 'axios';
import * as crypto from 'crypto';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  encriptarPayload(payloadData: any) {
    // const secretKey =
    //   '2d39b8f158ab1c62adea76e0a6ff87012e005a1dd0839bed595e639132283ce2';
    const secretKey = crypto.randomBytes(32).toString('hex');
    log(secretKey);
    // Encriptar el payload
    const payload = JSON.stringify(payloadData);
    log(payload);
    const iv = crypto.randomBytes(16); // Genera un vector de inicialización aleatorio
    log(iv);
    const cipher = crypto.createCipheriv(
      'aes-256-cbc',
      Buffer.from(secretKey),
      iv,
    );
    log(cipher);
    let encryptedPayload = cipher.update(payload, 'utf8', 'base64');
    encryptedPayload += cipher.final('base64');
    return encryptedPayload;
  }

  generaToken(payloadData: any) {
    // Clave secreta utilizada para firmar el token (debe mantenerse segura)
    const secretKey = 'tu_clave_secreta';
    // Genera el token JWS
    const payload = JSON.stringify(payloadData);
    const token = jwt.sign(payload, secretKey);
    const resp = this.separaToken(token);
    return resp;
  }

  separaToken(token: any) {
    // Separar el token en partes para obtener header y signature
    const parts = token.split('.');
    const header = parts[0];
    const signature = parts[2];
    return {
      header,
      signature,
    };
  }

  async consumeSecondProject() {
    const secondProjectBaseUrl = 'http://localhost:3001'; // Reemplaza con la URL de tu segundo proyecto
    const consumeServiceUrl = `${secondProjectBaseUrl}/consume`;

    try {
      // Datos que deseas incluir en el payload
      const payloadData = {
        username: 'vico',
        pass: '123123',
      };
      // const payloadEncrypt = this.encriptarPayload(payloadData); // encrypta payload
      const { header, signature } = this.generaToken(payloadData); // genera token

      // Formatear los datos
      const keyname = 'prueba-key';
      const jwsData = {
        keyname,
        payload: payloadData,
        header,
        signature,
      };
      log('jwsData: ', jwsData);
      return jwsData;

      const response = await axios.post(consumeServiceUrl, jwsData);
      log('response: ', response);
      return response.data;
    } catch (error) {
      throw error;
    }
  }
}
