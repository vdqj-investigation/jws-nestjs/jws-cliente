import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import * as jwt from 'jsonwebtoken';
import { log } from 'console';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('sign')
  signData(@Body() data: any) {
    // La clave secreta utilizada para firmar y verificar
    const secretKey = 'mySecretKey';

    // Firma el objeto JSON y genera un token JWS
    const token = jwt.sign(data, secretKey);

    return { token };
  }

  @Post('verify')
  verifyToken(@Body() token: any) {
    // La clave secreta utilizada para verificar el token
    const secretKey = 'mySecretKey';

    try {
      // Verifica el token y obtén los datos
      const data = jwt.verify(token.token + '', secretKey);
      return { data };
    } catch (error) {
      return { error: 'Token no válido' };
    }
  }

  @Get('generate')
  generateJwsToken() {
    log('hola');
    return this.appService.consumeSecondProject();
  }
}
